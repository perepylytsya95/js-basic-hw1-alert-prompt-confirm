let userName;
do {
    userName = prompt("What's your name?");
} while (userName === "" || !userName);

let userAge = +prompt("How old are you?");
while(Number.isNaN(userAge) || userAge <= 0){
    alert("I need your age to continue. Your age should be a number");
    userAge = +prompt("How old are you?");
}

if(userAge < 18) {
    alert("You are not allowed to visit this website");
}
else if(userAge <= 22){
    let legalAge = confirm("Are you sure you want to continue?");
    if (legalAge){
        alert("Welcome " + userName);
    } else {
        alert("You are not allowed to visit this website");
    }
}
else if (userAge > 22){
    alert("Welcome " + userName);
}